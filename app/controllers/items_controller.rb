class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  # GET /items
  # GET /items.json
  def index
    @item = Item.new
    @items = Item.find(:all, :order => "picture")
  end

  # GET /items/1
  # GET /items/1.json
  def show
  end

  def select
    @item = Item.find(params[:id])
    @item.touch
    respond_to do |format|
      if @item.save
        format.html { redirect_to items_url }
        format.json { render action: 'show', status: :created, location: @item }
      else
        format.html { redirect_to items_url }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end
  api :POST, '/api/createFromAPI/'
  param :color, String, "screen color"
  param :picture, String, "picture url"
  formats ['json']
  def createFromAPI
    puts params.to_json
    @item = Item.new()
    @item.color = params[:color]
    @item.picture = params[:picture]
    @item.save
  end

  def create
    @item = Item.new(item_params)

    respond_to do |format|
      if @item.save
        format.html { redirect_to items_url }
        format.json { render action: 'show', status: :created, location: @item }
      else
        format.html { render action: 'new' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:color, :picture)
    end
end
