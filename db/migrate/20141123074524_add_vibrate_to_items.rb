class AddVibrateToItems < ActiveRecord::Migration
  def change
    add_column :items, :vibrate, :boolean, default: false
  end
end
