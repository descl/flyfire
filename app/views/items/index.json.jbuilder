json.array!(@items) do |item|
  json.extract! item, :id, :color, :picture
  json.url item_url(item, format: :json)
end
